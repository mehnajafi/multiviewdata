clear
load 3sources_bbc.docs
bbc_list = X3sources_bbc;

load 3sources_guardian.docs
guardian_list = X3sources_guardian;
load 3sources_reuters.docs
reuters_list = X3sources_reuters;
[A, ncol, nrow, nentries] = readmtx('3sources_reuters.mtx');
reuters_matrix = A';
[A, ncol, nrow, nentries] = readmtx('3sources_guardian.mtx');
guardian_matrix = A';
[A, ncol, nrow, nentries] = readmtx('3sources_bbc.mtx');
bbc_matrix = A';

list_unique = unique([bbc_list;guardian_list;reuters_list]);
label_disjoint = zeros(size(list_unique,1),1);
i_1 = [001,013,015,016,023,024,032,033,034,035,036,039,056,057,064,075,079,080,081,091,092,096,097,110,112,116,117,124,125,128,136,137,141,144,150,155,160,163,166,167,171,177,180,183,187,191,192,193,194,200,213,215,216,222,224,226,227,234,236,241,242,243,246,258,259,260,261,262,268,270,271,274,275,277,279,281,282,284,297,298,304,325,326,327,328,329,335,346,348,351,357,360,365,366,373,374,379,381,382,383,387,388,391,402];
i_2 = [005,014,025,026,028,037,038,048,049,058,059,060,066,071,076,077,078,098,099,100,101,105,129,130,131,142,165,176,190,207,208,209,217,228,229,235,248,249,251,272,278,283,292,293,300,321,324,339,347,349,350,359,361,362,364,376,377,384,405,411];
i_3 = [042,065,090,109,113,120,135,145,146,152,156,168,172,173,174,178,179,181,182,185,186,202,203,204,205,206,230,231,247,265,267,273,290,299,301,313,331,332,333,334,352,353,354,363,386,394,397,398,399,400,404,408,415,416];
i_4 = [004,017,018,040,041,061,062,063,067,068,073,074,095,108,114,118,119,122,140,143,154,169,211,212,214,225,240,255,256,257,289,295,307,308,309,310,311,312,320,330,340,341,342,358,372,375,401,406,407];
i_5 = [006,011,012,019,020,021,022,027,029,050,051,052,053,054,055,082,083,084,085,086,093,094,106,107,111,123,126,127,138,139,151,153,157,158,159,170,175,184,195,196,197,198,199,220,221,223,232,233,237,238,239,244,245,263,264,269,285,286,287,288,294,296,302,303,314,315,316,317,318,319,322,323,343,344,345,355,368,369,370,371,389,390,393,395,396,409,410,412,413];
i_6 = [002,003,007,008,009,010,030,031,043,044,045,046,047,069,070,072,087,088,089,102,103,104,115,121,132,133,134,147,148,149,161,162,164,188,189,201,210,218,219,250,252,253,254,266,276,280,291,305,306,336,337,338,356,367,378,380,385,392,403,414];

label_all = zeros(numel(list_unique),1);
label_all(i_1) = 1;
label_all(i_2) = 2;
label_all(i_3) = 3;
label_all(i_4) = 4;
label_all(i_5) = 5;
label_all(i_6) = 6;

label_bbc = label_all(bbc_list);
label_reuters = label_all(reuters_list);
label_guardian = label_all(guardian_list);

[~,~,i1] = union(reuters_list, bbc_list);
[~,~,i2] = union(bbc_list, reuters_list);
[C_i,ia,ib] = intersect(bbc_list, reuters_list);
c = numel(C_i);
a = numel(i1);
b = numel(i2);
bbc_m_c = bbc_matrix(ia,:);
bbc_m_a = bbc_matrix(i1,:);
reuters_m_c = reuters_matrix(ib,:);
reuters_m_b = reuters_matrix(i2,:);
label_c = label_bbc(ia);
label_a = label_bbc(i1);
label_b = label_reuters(i2);
save('bbc_reuters_incomp.mat','bbc_m_a','bbc_m_c','reuters_m_b','reuters_m_c','label_c','label_b','label_a','a','b','c');

[~,~,i1] = union(guardian_list, bbc_list);
[~,~,i2] = union(bbc_list, guardian_list);
[C_i,ia,ib] = intersect(bbc_list, guardian_list);
c = numel(C_i);
a = numel(i1);
b = numel(i2);
bbc_m_c = bbc_matrix(ia,:);
bbc_m_a = bbc_matrix(i1,:);
guardian_m_c = guardian_matrix(ib,:);
guardian_m_b = guardian_matrix(i2,:);
label_c = label_bbc(ia);
label_a = label_bbc(i1);
label_b = label_guardian(i2);
save('bbc_guardian_incomp.mat','bbc_m_a','bbc_m_c','guardian_m_b','guardian_m_c','label_c','label_b','label_a','a','b','c');

[~,~,i1] = union(guardian_list, reuters_list);
[~,~,i2] = union(reuters_list, guardian_list);
[C_i,ia,ib] = intersect(reuters_list, guardian_list);
c = numel(C_i);
a = numel(i1);
b = numel(i2);
reuters_m_c = reuters_matrix(ia,:);
reuters_m_a = reuters_matrix(i1,:);
guardian_m_c = guardian_matrix(ib,:);
guardian_m_b = guardian_matrix(i2,:);
label_c = label_reuters(ia);
label_a = label_reuters(i1);
label_b = label_guardian(i2);
save('reuters_guardian_incomp.mat','reuters_m_a','reuters_m_c','guardian_m_b','guardian_m_c','label_c','label_b','label_a','a','b','c');

clear
load 3sources_bbc.docs
bbc_list = X3sources_bbc;

load 3sources_guardian.docs
guardian_list = X3sources_guardian;
load 3sources_reuters.docs
reuters_list = X3sources_reuters;
[A, ncol, nrow, nentries] = readmtx('3sources_reuters.mtx');
reuters_matrix = A';
[A, ncol, nrow, nentries] = readmtx('3sources_guardian.mtx');
guardian_matrix = A';
[A, ncol, nrow, nentries] = readmtx('3sources_bbc.mtx');
bbc_matrix = A';

list_unique = unique([bbc_list;guardian_list;reuters_list]);
label_disjoint = zeros(size(list_unique,1),1);
i_1 = [001,013,015,016,023,024,032,033,034,035,036,039,056,057,064,075,079,080,081,091,092,096,097,110,112,116,117,124,125,128,136,137,141,144,150,155,160,163,166,167,171,177,180,183,187,191,192,193,194,200,213,215,216,222,224,226,227,234,236,241,242,243,246,258,259,260,261,262,268,270,271,274,275,277,279,281,282,284,297,298,304,325,326,327,328,329,335,346,348,351,357,360,365,366,373,374,379,381,382,383,387,388,391,402];
i_2 = [005,014,025,026,028,037,038,048,049,058,059,060,066,071,076,077,078,098,099,100,101,105,129,130,131,142,165,176,190,207,208,209,217,228,229,235,248,249,251,272,278,283,292,293,300,321,324,339,347,349,350,359,361,362,364,376,377,384,405,411];
i_3 = [042,065,090,109,113,120,135,145,146,152,156,168,172,173,174,178,179,181,182,185,186,202,203,204,205,206,230,231,247,265,267,273,290,299,301,313,331,332,333,334,352,353,354,363,386,394,397,398,399,400,404,408,415,416];
i_4 = [004,017,018,040,041,061,062,063,067,068,073,074,095,108,114,118,119,122,140,143,154,169,211,212,214,225,240,255,256,257,289,295,307,308,309,310,311,312,320,330,340,341,342,358,372,375,401,406,407];
i_5 = [006,011,012,019,020,021,022,027,029,050,051,052,053,054,055,082,083,084,085,086,093,094,106,107,111,123,126,127,138,139,151,153,157,158,159,170,175,184,195,196,197,198,199,220,221,223,232,233,237,238,239,244,245,263,264,269,285,286,287,288,294,296,302,303,314,315,316,317,318,319,322,323,343,344,345,355,368,369,370,371,389,390,393,395,396,409,410,412,413];
i_6 = [002,003,007,008,009,010,030,031,043,044,045,046,047,069,070,072,087,088,089,102,103,104,115,121,132,133,134,147,148,149,161,162,164,188,189,201,210,218,219,250,252,253,254,266,276,280,291,305,306,336,337,338,356,367,378,380,385,392,403,414];

label_all = zeros(numel(list_unique),1);
label_all(i_1) = 1;
label_all(i_2) = 2;
label_all(i_3) = 3;
label_all(i_4) = 4;
label_all(i_5) = 5;
label_all(i_6) = 6;

bbc_m = zeros(numel(list_unique), size(bbc_matrix,2));
reuters_m = zeros(numel(list_unique), size(reuters_matrix,2));
guardian_m = zeros(numel(list_unique), size(guardian_matrix,2));
bbc_m(bbc_list,:) = bbc_matrix;
reuters_m(reuters_list,:) = reuters_matrix;
guardian_m(guardian_list,:) = guardian_matrix;
ind_bbc = false(numel(list_unique),1);
ind_bbc(bbc_list) = true;
ind_reuters = false(numel(list_unique),1);
ind_reuters(reuters_list) = true;
ind_guardian = false(numel(list_unique),1);
ind_guardian(guardian_list) = true;
bbc_m(~ind_bbc,:) = repmat(mean(bbc_matrix),sum(~ind_bbc),1);
reuters_m(~ind_reuters,:) = repmat(mean(reuters_matrix), sum(~ind_reuters),1);
guardian_m(~ind_guardian,:) = repmat(mean(guardian_matrix), sum(~ind_guardian),1);
save('3sources_incomp.mat','bbc_m','reuters_m','guardian_m','label_all', 'ind_bbc','ind_reuters','ind_guardian');
