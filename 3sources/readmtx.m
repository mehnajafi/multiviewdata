function [A, ncol, nrow, nentries] = readmtx(filename)
    fp = fopen(filename);
    str = fgets(fp);
    while regexp(str, '^%')
        str = fgets(fp);
    end
    s = sscanf(str, '%d %d %d');
    ncol = s(2);
    nrow = s(1);
    nentries = s(3);
    A = sparse(zeros(nrow, ncol));
    for i = 1:nentries
        s = sscanf(fgets(fp), '%d %d %d');
        A(s(1),s(2)) = s(3);
    end
end